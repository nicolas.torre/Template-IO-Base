export const paymentMethodGroups = {
    creditCardPaymentGroup: "Tarjeta de crédito",
    debitCardPaymentGroup: "Tarjeta de débito",
    promissoryPaymentGroup: "Pago en sucursal",
    MercadoPagoProPaymentGroup: "Mercado Pago",
    cardPromissoryPaymentGroup: "Transferencia bancaria",
    // hasta aca trae datos
    bankInvoicePaymentGroup: "Pago en efectivo",
    customPrivate_501PaymentGroup: "Tarjeta Cordobesa",
    customPrivate_502PaymentGroup: "Tarjeta Cordobesa",
    customPrivate_503PaymentGroup: "Tarjeta CMR",
    customPrivate_504PaymentGroup: "Tarjeta Cordial",
    custom201PaymentGroupPaymentGroup: "Transferencia bancaria",
    custom202PaymentGroupPaymentGroup: "Pago en sucursal"
  }
export const defaultValues = {
    creditCardPaymentGroup: "Visa",
    debitCardPaymentGroup: "Visa Electron",
    promissoryPaymentGroup: "CardPromissory",
    MercadoPagoProPaymentGroup: "Mercado Pago",
    bankInvoicePaymentGroup: "RAPIPAGO",
    customPrivate_501PaymentGroup: "Tarjeta Cordobesa",
    customPrivate_502PaymentGroup: "Tarjeta Cordobesa",
    customPrivate_503PaymentGroup: "Tarjeta CMR",
    customPrivate_504PaymentGroup: "Tarjeta Cordial",
    //Estos valores hay que ver cómo vienen en la info para cambiar los datos en el front
    custom201PaymentGroupPaymentGroup: "Transferencia bancaria",
    custom202PaymentGroupPaymentGroup: "Pago en sucursal"
  }
  
export const paymentOptionsIcons = {
    'Mastercard Debit': 'https://merlinosrl.vteximg.com.br/arquivos/icono-mastercard-modal.png',
    'Visa': 'https://merlinosrl.vteximg.com.br/arquivos/icono-visa-modal.png',
    'Mastercard': 'https://merlinosrl.vteximg.com.br/arquivos/icono-mastercard-modal.png',
    'Nativa': 'https://merlinosrl.vteximg.com.br/arquivos/nativa.png',
    'Diners': 'https://merlinosrl.myvtex.com/arquivos/icono-diners-modal.png',
    'Cabal': 'https://merlinosrl.myvtex.com/arquivos/icono-cabal-modal.png',
    'Nevada': 'https://merlinosrl.myvtex.com/arquivos/icono-nevada-modal.png',
    'Shopping':'https://merlinosrl.myvtex.com/arquivos/icono-shopping-modal.png',
    'Naranja': 'https://merlinosrl.vteximg.com.br/arquivos/icono-naranja-modal.png',
    'Visa Electron': 'https://merlinosrl.vteximg.com.br/arquivos/logo-visaelectron-modal.png',
    'MercadoPagoPro': 'https://merlinosrl.vteximg.com.br/arquivos/logo-mp-modal.png',
    'Promissory': 'https://merlinosrl.vteximg.com.br/arquivos/icono-tarjeta-modal.png',
    'Maestro': 'https://merlinosrl.vteximg.com.br/arquivos/logo-maestro-modal.png',
    'American Express': 'https://merlinosrl.vteximg.com.br/arquivos/logo-american-modal.png',
    'PAGOFACIL': 'https://merlinosrl.vteximg.com.br/arquivos/logo-pagofacil-modal.png',
    'RAPIPAGO': 'https://merlinosrl.vteximg.com.br/arquivos/logo-rapipago-modal.png',
    'Tarjeta Cencosud': 'https://merlinosrl.vteximg.com.br/arquivos/logo-cencosud-modal.png',
    'Tarjeta Cordobesa': 'https://merlinosrl.vteximg.com.br/arquivos/logo-cordobesa-modal.png',
    'Tarjeta CMR': 'https://merlinosrl.vteximg.com.br/arquivos/logo-cmr-modal.png',
    'Tarjeta Cordial': 'https://merlinosrl.vteximg.com.br/arquivos/logo-cordial-modal.png',
  }
  